package com.gomel.tat.home14.tests.disk;

import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.service.DiskCheckService.finalDeleteCheck;
import static com.gomel.tat.home14.lib.service.DiskDeleteService.finalDeleteFiles;

public class FinalDeleteTest extends TrashTest {

    @Test(dependsOnMethods = "moveFilesToTrash", description = "Method delete test files from trash " +
            "and check that the delete notification is displayed")
    public void finalDeleteTest() {
        finalDeleteFiles(driver, testFiles);
        Assert.assertTrue(finalDeleteCheck(), "Trash file does not finally deleted");
    }
}
