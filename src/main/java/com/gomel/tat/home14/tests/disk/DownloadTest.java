package com.gomel.tat.home14.tests.disk;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static com.gomel.tat.home14.lib.service.DiskCheckService.isCheckSumsMatch;
import static com.gomel.tat.home14.lib.service.DiskCheckService.isFilesExists;
import static com.gomel.tat.home14.lib.service.DiskService.*;

public class DownloadTest extends UploadTest {

    @Test(dependsOnMethods = "uploadTest", description = "Method download test file and check that it exists")
    public void downloadTest() {
        downloadFiles(driver, testFiles);
        Boolean result = isFilesExists(testFiles);
        Assert.assertTrue(result, "File does not exist in destination folder");
    }

    @Test(dependsOnMethods = "downloadTest", description = "Method check that is test file content equals result file content")
    public void checkFilesContent() {
        List<String> actualFileCheckSum = getCheckSumOfFile(testFiles);
        List<String> expectedFileCheckSum = getCheckSumOfFile(getDownloadedFiles(testFiles));
        Assert.assertTrue(isCheckSumsMatch(actualFileCheckSum, expectedFileCheckSum), "Error. File not equal");
    }
}
