package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.common.bo.Letter;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.AbstractMailListPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.ComposePage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.DraftPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.TrashPage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.common.bo.LetterFactory.getRandomLetter;
import static com.gomel.tat.home14.lib.service.MailCheckService.checkIsDraftMailDisplayed;
import static com.gomel.tat.home14.lib.service.MailCheckService.isElementDisplayed;
import static com.gomel.tat.home14.lib.service.MailService.*;

public class DraftMailTest extends TestPreparations {
    private String draftId;

    @Test(description = "Method login to yandex mail, compose random mail and check while mail appear in draft")
    public void createAndCheckDraft() {
        Letter letter = getRandomLetter();
        loginToMail();
        new ComposePage(driver).open();
        createDraft(driver, letter);
        draftId = getDraftId(driver);
        new DraftPage(driver).open();
        Assert.assertTrue(checkIsDraftMailDisplayed(draftId), "Letter should be present in draft folder");
    }

    @Test(dependsOnMethods = "createAndCheckDraft", description = "Method delete draft " +
            "and check the delete notification is displayed")
    public void deleteAndCheckDeletedDraft() {
        deleteMail(draftId);
        Assert.assertTrue(isElementDisplayed(AbstractMailListPage.DELETE_NOTIFICATION_LOCATOR), "Letter does't " +
                "deleted from draft folder");
    }

    @Test(dependsOnMethods = "deleteAndCheckDeletedDraft", description = "Method open trash pages, delete draft " +
            "and check the delete notification is displayed")
    public void finalDeleteDraft() {
        new TrashPage(driver).open();
        deleteMail(draftId);
        Assert.assertTrue(isElementDisplayed(AbstractMailListPage.DELETE_NOTIFICATION_LOCATOR), "Letter does not deleted from trash folder");
    }
}
