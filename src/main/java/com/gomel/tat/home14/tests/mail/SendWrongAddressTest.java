package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.ui.screen.yandexmail.ComposePage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.common.bo.LetterFactory.getLetterWithWrongAddress;
import static com.gomel.tat.home14.lib.service.MailCheckService.isElementDisplayed;
import static com.gomel.tat.home14.lib.service.MailService.sendLetter;

public class SendWrongAddressTest extends TestPreparations {

    @Test(description = "Method login to yandexmail, compose mail using wrong mailTo field " +
            "and check the error notification is displayed")
    public void sendWrongMail() {
        loginToMail();
        new ComposePage(driver).open();
        sendLetter(driver, getLetterWithWrongAddress());
        Assert.assertTrue(isElementDisplayed(ComposePage.ERROR_NOTIFICATION_LOCATOR), "Did't fill mailto field using bad address");
    }
}
