package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.ui.screen.yandexmail.MailLoginPage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.common.bo.AccountFactory.getAccountWithWrongPassword;
import static com.gomel.tat.home14.lib.service.LoginMailService.failureLoginToMailAccount;
import static com.gomel.tat.home14.lib.service.MailCheckService.isElementDisplayed;

public class NegativeLoginTest extends TestPreparations {

    @Test(description = "Method try to login into yandexmail using wrong password " +
            "and check the error message is displayed.")
    public void negativeLogin() {
        new MailLoginPage(driver).open();
        failureLoginToMailAccount(driver, getAccountWithWrongPassword());
        Assert.assertTrue(isElementDisplayed(MailLoginPage.WRONG_LOGIN_ERROR_LOCATOR), "Error! Login success");
    }
}
