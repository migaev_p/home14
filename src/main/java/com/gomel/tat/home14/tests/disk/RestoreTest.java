package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.service.DiskService;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.FilesPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.service.DiskCheckService.isFilesDisplayed;

public class RestoreTest extends TrashTest {

    @Test(dependsOnMethods = "moveFilesToTrash", description = "Method restore deleted to trash file/files into disk")
    public void restoreTest() {
        DiskService.restoreTestFilesFromTrash(driver, testFiles);
        new FilesPage(driver).open();
        Assert.assertTrue(isFilesDisplayed(testFiles, FilesPage.UPLOADED_FILE_LOCATOR_PATTERN), "File/files does not restored");
    }
}
