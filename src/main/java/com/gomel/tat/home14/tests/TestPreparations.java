package com.gomel.tat.home14.tests;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.service.DiskLoginService;
import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.DiskLoginPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.MailLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;

import static com.gomel.tat.home14.lib.common.bo.AccountFactory.getDefaultAccount;
import static com.gomel.tat.home14.lib.service.LoginMailService.loginToMailAccount;

public class TestPreparations {
    protected WebDriver driver;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver(GlobalConfig.getInstance().getBrowserType());
    }

    public void loginYandexDisk() {
        new DiskLoginPage(driver).open();
        DiskLoginService.loginToDiskAccount(driver, getDefaultAccount());
    }

    public void loginToMail() {
        new MailLoginPage(driver).open();
        loginToMailAccount(driver, getDefaultAccount());
    }
}
