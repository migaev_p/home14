package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.ui.screen.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.service.DiskCheckService.isFilesDisplayed;
import static com.gomel.tat.home14.lib.service.DiskDeleteService.deleteFilesToTrash;


public class TrashTest extends UploadTest {

    @Test(dependsOnMethods = "uploadTest", description = "Method drag and drop test file/files to trash " +
            "and check the file/files is displayed in trash box")
    public void moveFilesToTrash() {
        deleteFilesToTrash(driver, testFiles);
        new TrashPage(driver).open();
        Assert.assertTrue(isFilesDisplayed(testFiles, TrashPage.DELETED_FILE_LOCATOR_PATTERN), "Deleted file/files does't exists in trash folder");
    }
}
