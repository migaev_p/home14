package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.common.bo.Letter;
import com.gomel.tat.home14.lib.common.bo.LetterFactory;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.*;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.security.InvalidParameterException;

import static com.gomel.tat.home14.lib.service.MailCheckService.isElementDisplayed;
import static com.gomel.tat.home14.lib.service.MailCheckService.isLetterMatch;
import static com.gomel.tat.home14.lib.service.MailService.sendLetter;

public class SendMailTest extends TestPreparations {
    private Letter letter;
    private static final String PROPERLY_MAIL = "Properly";
    private static final String EMPTY_MAIL = "Empty";

    @BeforeClass(description = "Method create random or empty letter using parameter kindOfLetter")
    @Parameters("kindOfLetter")
    public void prepareData(String kindOfLetter) {
        switch (kindOfLetter) {
            case PROPERLY_MAIL:
                letter = LetterFactory.getRandomLetter();
                break;
            case EMPTY_MAIL:
                letter = LetterFactory.getEmptyLetter();
                break;
            default:
                throw new InvalidParameterException("Invalid mail parameter!");
        }
    }

    @Test(description = "Method login to yandexmail, compose random or empty letter," +
            " open inbox folder and check is the sent letter present")
    @Parameters("kindOfLetter")
    public void sendMail(String kindOfLetter) {
        loginToMail();
        new ComposePage(driver).open();
        sendLetter(driver, letter);
        isElementDisplayed(AbstractMailListPage.MAIL_ARRIVED_NOTIFICATION_LOCATOR);
        new InboxPage(driver).open();
        new SpecifiedLetterPage(driver).openFirstLetter();
        Assert.assertTrue(isLetterMatch(driver, letter), kindOfLetter + "letter should be present in inbox pages");
    }

    @Test(dependsOnMethods = "sendMail", description = "Method open sent mail folder " +
            "and check is the mail sent before present in this folder")
    @Parameters("kindOfLetter")
    public void checkSentMail(String kindOfLetter) {
        new SentPage(driver).open();
        new SpecifiedLetterPage(driver).openFirstLetter();
        Assert.assertTrue(isLetterMatch(driver, letter), kindOfLetter + "letter should be present in sent pages");
    }
}
