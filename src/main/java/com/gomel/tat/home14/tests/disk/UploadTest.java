package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.common.bo.FileFactory;
import com.gomel.tat.home14.lib.service.DiskService;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.FilesPage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.home14.lib.service.DiskCheckService.isFilesDisplayed;

public class UploadTest extends TestPreparations {
    protected List<File> testFiles = new ArrayList<File>();

    @BeforeTest
    @Parameters("numberOfTestFiles")
    public void prepareData(int numberOfTestFiles) {
        testFiles = FileFactory.getTestFiles(numberOfTestFiles);
    }

    @Test(description = "Method login to yandex disk, upload one test file and check the file is displayed on the disk")
    public void uploadTest() {
        loginYandexDisk();
        DiskService.uploadTestFiles(driver, testFiles);
        Assert.assertTrue(isFilesDisplayed(testFiles, FilesPage.UPLOADED_FILE_LOCATOR_PATTERN), "File does not upload");
    }
}
