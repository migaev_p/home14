package com.gomel.tat.home14.tests.mail;

import com.gomel.tat.home14.lib.ui.screen.yandexmail.MailLoginPage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home14.lib.common.bo.AccountFactory.getDefaultAccount;
import static com.gomel.tat.home14.lib.service.LoginMailService.loginToMailAccount;
import static com.gomel.tat.home14.lib.service.MailCheckService.isElementDisplayed;

public class SuccessLoginTest extends TestPreparations {

    @Test(description = "Method login to yandexmail using properly user data " +
            "and check the user name on inbox pages is displayed")
    public void successLogin() {
        new MailLoginPage(driver).open();
        loginToMailAccount(driver, getDefaultAccount());
        Assert.assertTrue(isElementDisplayed(MailLoginPage.USER_NAME_LOCATOR), "Login is not success");
    }
}
