package com.gomel.tat.home14.tests.disk;

import com.gomel.tat.home14.lib.common.bo.FileFactory;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.TrashPage;
import com.gomel.tat.home14.tests.TestPreparations;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.home14.lib.service.DiskCheckService.isFilesDisplayed;
import static com.gomel.tat.home14.lib.service.DiskDeleteService.deleteFilesToTrash;
import static com.gomel.tat.home14.lib.service.DiskService.uploadTestFiles;

public class MultipleDeleteTest extends TestPreparations {
    private List<File> testFiles = new ArrayList<File>();

    @BeforeTest
    @Parameters("numberOfTestFiles")
    public void prepareData(int numberOfTestFiles) {
        testFiles = FileFactory.getTestFiles(numberOfTestFiles);
    }

    @Test(description = "Method login to yandex disk, upload test files, drag and drop their to trash " +
            "and check is that files present in trash box")
    public void multipleDelete() {
        loginYandexDisk();
        uploadTestFiles(driver, testFiles);
        deleteFilesToTrash(driver, testFiles);
        new TrashPage(driver).open();
        Assert.assertTrue(isFilesDisplayed(testFiles, TrashPage.DELETED_FILE_LOCATOR_PATTERN), "File does not upload");
    }
}
