package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.common.bo.Letter;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.AbstractMailListPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.ComposePage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.InboxPage;
import com.gomel.tat.home14.lib.report.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsClickable;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home14.lib.utils.Utils.getLocatorByPattern;

public class MailService {
    private static final String URL_SEPARATOR = "/";

    public static InboxPage sendLetter(WebDriver driver, Letter letter) {
        Logger.info("Compose letter: " + letter);
        ComposePage composePage = PageFactory.initElements(driver, ComposePage.class);
        fillLetter(driver, letter);
        screenshot();
        return composePage.submitSend();
    }

    public static ComposePage createDraft(WebDriver driver, Letter letter) {
        Logger.info("Create draft: " + letter);
        fillLetter(driver, letter);
        waitIsDraftSave();
        screenshot();
        return new ComposePage(driver);
    }

    public static void fillLetter(WebDriver driver, Letter letter) {
        Logger.debug("Fill letter: " + letter);
        ComposePage composePage = new ComposePage(driver);
        composePage.typeTo(letter.getRecipient());
        composePage.typeSubject(letter.getSubject());
        composePage.typeMailText(letter.getBody());
        screenshot();
    }

    private static void waitIsDraftSave() {
        Logger.debug("Wait is draft save. Locator: " + ComposePage.SAVE_DRAFT_LOCATOR);
        waitForElementIsVisible(ComposePage.SAVE_DRAFT_LOCATOR);
    }

    public static void deleteMail(String mailId) {
        Logger.debug("Delete mail by ID: " + mailId);
        waitForElementIsClickable(getLocatorByPattern(AbstractMailListPage.MAIL_CHECKBOX_LOCATOR_PATTERN, mailId)).click();
        waitForElementIsVisible(getLocatorByPattern(AbstractMailListPage.MAIL_CHECKBOX_IS_CHECKED_LOCATOR_PATTERN, mailId));
        waitForElementIsClickable(AbstractMailListPage.DELETE_BUTTON_LOCATOR).click();
        waitForElementIsVisible(AbstractMailListPage.DELETE_NOTIFICATION_LOCATOR);
        screenshot();
    }

    public static String getDraftId(WebDriver driver) {
        String currentUrl = driver.getCurrentUrl();
        Logger.debug("Draft ID: " + currentUrl.substring(getFirstCharIndexOfIdFromUrl(currentUrl)));
        return currentUrl.substring(getFirstCharIndexOfIdFromUrl(currentUrl));
    }

    private static Integer getFirstCharIndexOfIdFromUrl(String currentUrl) {
        int result = currentUrl.lastIndexOf(URL_SEPARATOR) + 1;
        Logger.debug("Get first char index before ID: " + result);
        return result;
    }
}
