package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class AbstractMailListPage extends Page {
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//div[@class ='b-statusline']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']//a[contains(@class, 'item_delete')]");
    public static final By MAIL_ARRIVED_NOTIFICATION_LOCATOR = By.xpath("//div[@class='b-statusline']/span[@class='b-statusline__content']");
    public static final String MAIL_CHECKBOX_LOCATOR_PATTERN = "//input[@type='checkbox' and @value='%s']";
    public static final String MAIL_CHECKBOX_IS_CHECKED_LOCATOR_PATTERN = "//div[contains(@class, 'b-messages__message_checked') and @data-id='%s']";
    public static final String SPECIFIED_LETTER_LOCATOR_PATTERN = "//div[@data-id='%s']";

    public AbstractMailListPage(WebDriver driver) {
        super(driver);
    }
}
