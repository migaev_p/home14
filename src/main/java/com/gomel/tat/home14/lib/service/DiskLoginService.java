package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.common.bo.Account;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.FilesPage;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.DiskLoginPage;
import com.gomel.tat.home14.lib.report.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;

public class DiskLoginService {

    public static FilesPage loginToDiskAccount(WebDriver driver, Account account) {
        return fillInputFields(driver, account).submitLogin();
    }

    private static DiskLoginPage fillInputFields(WebDriver driver, Account account) {
        Logger.info("Fill login input field");
        Logger.debug("Login: " + account.getLogin() + "Password: " + account.getPassword());
        DiskLoginPage diskLoginPage = PageFactory.initElements(driver, DiskLoginPage.class);
        diskLoginPage.typeUsername(account.getLogin());
        diskLoginPage.typePassword(account.getPassword());
        screenshot();
        return diskLoginPage;
    }
}
