package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.WebDriver;

public class DraftPage extends Page {
    public static final String partOfDraftUrl = "#draft";

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public DraftPage open() {
        driver.navigate().to(WebDriverHelper.getUrlBeforePageName(partOfDraftUrl));
        return this;
    }
}
