package com.gomel.tat.home14.lib.utils;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.report.Logger;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;

import static org.apache.commons.io.FileUtils.getFile;

public class Utils {
    private static final int DOWNLOAD_TIMEOUT_SECONDS = 30;
    private static final int CHECK_DELAY_MILLISECONDS = 1000;
    private static final int TIME_PERIOD = 1000;
    private static final String TEMPORARY_DIRECTORY = "temp";
    private static final String CHECK_ALGORITHM = "MD5";

    public static String getFilePath(String path, String fileName) {
        return path.concat(File.separator).concat(fileName);
    }

    public static String getCheckSumOfFile(File file) throws Exception {
        Logger.info("Get check sum of file");
        MessageDigest messageDigest = MessageDigest.getInstance(CHECK_ALGORITHM);
        byte[] dataBytes = new byte[1024];
        int readByte;
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            while ((readByte = fileInputStream.read(dataBytes)) != -1) {
                messageDigest.update(dataBytes, 0, readByte);
            }
        }
        byte[] messageDigestBytes = messageDigest.digest();
        StringBuilder hexString = new StringBuilder();
        for (byte messageDigestByte : messageDigestBytes) {
            String hex = Integer.toHexString(0xff & messageDigestByte);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        Logger.debug("Digest(in hex format):: " + hexString.toString());
        return hexString.toString();
    }

    public static void waitForDownloadFile(String filePath, String downloadExtension) {
        Logger.info("Wait while file download");
        int timeCount = 0;
        String filePathWithExtension = filePath.concat(downloadExtension);
        while (getFile(filePathWithExtension).exists() && !getFile(filePathWithExtension).isDirectory() && timeCount <= DOWNLOAD_TIMEOUT_SECONDS) {
            int delay = CHECK_DELAY_MILLISECONDS / TIME_PERIOD;
            timeCount += delay;
            try {
                Thread.sleep(CHECK_DELAY_MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (timeCount == DOWNLOAD_TIMEOUT_SECONDS) {
            Logger.debug("Download file timeout! " + timeCount);
        }
    }

    public static By getLocatorByPattern(String locatorPattern, String parameter) {
        return By.xpath(String.format(locatorPattern, parameter));
    }

    public static String getTestFilePath(File file) {
        return file.getAbsolutePath();
    }

    public static String getTempDirectory() {
        //try {
            return GlobalConfig.getInstance().getResultDir() + File.separator + TEMPORARY_DIRECTORY;/*Files.createTempDirectory(TEMPORARY_DIRECTORY).toAbsolutePath().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new InvalidParameterException("Create temporary directory error!");*/
    }

    public static String getRandomContent(int numberOfChars) {
        return RandomStringUtils.randomAlphanumeric(numberOfChars);
    }

    public static void pasteFromClipboard() {
        Robot robot;
        try {
            robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void setSysClipboardText(String text) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(text);
        clipboard.setContents(transferable, null);
    }
}
