package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.common.bo.Letter;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.AbstractMailListPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.SpecifiedLetterPage;
import com.gomel.tat.home14.lib.report.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home14.lib.utils.Utils.getLocatorByPattern;

public class MailCheckService {
    private static final String EMPTY_STRING = "";
    private static final String DEFAULT_EMPTY_SUBJECT = "(Без темы)";
    private static final String DEFAULT_EMPTY_BODY = " ";

    public static boolean isLetterMatch(WebDriver driver, Letter letter) {
        Logger.info("Check is letter match");
        //String resultRecipient = driver.findElement(SpecifiedLetterPage.MAIL_RECIPIENT_LOCATOR).getText();
        String resultSubject = driver.findElement(SpecifiedLetterPage.MAIL_SUBJECT_LOCATOR).getText();
        String resultBody = driver.findElement(SpecifiedLetterPage.MAIL_BODY_LOCATOR).getText();
        //String recipient = letter.getRecipient();
        String subject = letter.getSubject();
        String body = letter.getBody();
        if (subject.equals(EMPTY_STRING)) {
            subject = DEFAULT_EMPTY_SUBJECT;
        }
        if (body.equals(EMPTY_STRING)) {
            body = DEFAULT_EMPTY_BODY;
        }
        //System.out.println("Result recipient: " + resultRecipient);
        //System.out.println("Aclual recipient: " + recipient);
        Logger.debug("Result subject: " + resultSubject);
        Logger.debug("Aclual subject: " + subject);
        Logger.debug("Result body: " + resultBody);
        Logger.debug("Aclual body: " + body);
        return resultSubject.equals(subject) && resultBody.equals(body);
    }

    public static Boolean isElementDisplayed(By locator) {
        Logger.debug("Check is element displayed. Locator: " + locator);
        screenshot();
        return waitForElementIsVisible(locator).isDisplayed();
    }

    public static Boolean checkIsDraftMailDisplayed(String mailId) {
        By specifiedLetterLocator = getLocatorByPattern(AbstractMailListPage.SPECIFIED_LETTER_LOCATOR_PATTERN, mailId);
        Logger.debug("Check is draft mail displayed. Locator: " + specifiedLetterLocator);
        screenshot();
        return waitForElementIsVisible(specifiedLetterLocator).isDisplayed();
    }
}