package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.common.CommonConstants.MAILBOX_URL;

public class MailLoginPage extends Page {
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By USER_NAME_LOCATOR = By.xpath("//*[contains(@class, 'user-name')]");
    public static final By WRONG_LOGIN_ERROR_LOCATOR = By.xpath("//div[@class='error-msg']");

    public MailLoginPage(WebDriver driver) {
        super(driver);
    }

    public MailLoginPage open() {
        driver.get(MAILBOX_URL);
        return this;
    }

    public MailLoginPage typeUsername(String username) {
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(username);
        return this;
    }

    public MailLoginPage typePassword(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public InboxPage submitLogin() {
        driver.findElement(PASSWORD_INPUT_LOCATOR).submit();
        return new InboxPage(driver);
    }
}
