package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;

public class InboxPage extends Page {
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#inbox']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage open() {
        waitForElementIsVisible(INBOX_LINK_LOCATOR).click();
        return this;
    }
}
