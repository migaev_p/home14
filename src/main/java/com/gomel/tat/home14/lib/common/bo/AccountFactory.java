package com.gomel.tat.home14.lib.common.bo;

import static com.gomel.tat.home14.lib.common.CommonConstants.DEFAULT_USER_LOGIN;
import static com.gomel.tat.home14.lib.common.CommonConstants.DEFAULT_USER_PASSWORD;
import static com.gomel.tat.home14.lib.utils.Utils.getRandomContent;

public class AccountFactory {
    private static final int SIZE_OF_POSTFIX = 3;

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_USER_LOGIN);
        account.setPassword(DEFAULT_USER_PASSWORD);
        return account;
    }

    public static Account getAccountWithWrongPassword() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + getRandomContent(SIZE_OF_POSTFIX));
        return account;
    }
}
