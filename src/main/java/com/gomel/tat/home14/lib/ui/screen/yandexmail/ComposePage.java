package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsClickable;
import static com.gomel.tat.home14.lib.utils.Utils.pasteFromClipboard;
import static com.gomel.tat.home14.lib.utils.Utils.setSysClipboardText;

public class ComposePage extends Page {
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By MAILTO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    public static final By ERROR_NOTIFICATION_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_invalid')]");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By SAVE_DRAFT_LOCATOR = By.xpath("//div[contains(@data-compose-type,'letter postcard')]//span[contains(@class,'actions__helper__date')]");


    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage open() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR).click();
        return this;
    }

    public ComposePage typeTo(String mailTo) {
        driver.findElement(MAILTO_INPUT_LOCATOR).sendKeys(mailTo);
        return this;
    }

    public ComposePage typeSubject(String subject) {
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(subject);
        return this;
    }

    public ComposePage typeMailText(String mailText) {
        setSysClipboardText(mailText);
        driver.findElement(MAIL_TEXT_LOCATOR).click();
        pasteFromClipboard();
        screenshot();
        return this;
    }

    public InboxPage submitSend() {
        waitForElementIsClickable(ComposePage.SEND_MAIL_BUTTON_LOCATOR).click();
        return new InboxPage(driver);
    }
}
