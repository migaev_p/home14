package com.gomel.tat.home14.lib.ui.screen.yandexdisk;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;

public class DiskLoginPage extends Page {
    public static final By USER_NAME_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    public static final By USER_NAME_LOCATOR = By.xpath("//*[contains(@class, 'username')]");
    public static final String BASE_URL = "https://disk.yandex.ru/";

    public DiskLoginPage(WebDriver driver) {
        super(driver);
    }

    public DiskLoginPage open() {
        driver.get(BASE_URL);
        return this;
    }

    public DiskLoginPage typeUsername(String username) {
        driver.findElement(USER_NAME_INPUT_LOCATOR).sendKeys(username);
        return this;
    }

    public DiskLoginPage typePassword(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public FilesPage submitLogin() {
        driver.findElement(PASSWORD_INPUT_LOCATOR).submit();
        waitForElementIsVisible(USER_NAME_LOCATOR);
        return new FilesPage(driver);
    }
}
