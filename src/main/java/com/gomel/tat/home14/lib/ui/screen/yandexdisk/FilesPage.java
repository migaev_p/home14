package com.gomel.tat.home14.lib.ui.screen.yandexdisk;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.*;

public class FilesPage extends Page {
    public static final By INPUT_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    public static final By UPLOAD_DONE_LOCATOR = By.xpath("//div[contains(@class, 'upload__icon_done')]");
    public static final By DIALOG_CLOSE_BUTTON_LOCATOR = By.xpath("//button[contains(@class, 'button-close')]");
    public static final By TRASH_BOX_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    public static final By FILES_LINK_LOCATOR = By.xpath("//div[@class='header__side-left']/a[2]");
    public static final By DELETE_NOTIFICATION_LOCATOR = By.xpath("//a[@href='/client/trash']/div");
    public static final String UPLOADED_FILE_LOCATOR_PATTERN = "//div[@data-id='/disk/%s']";
    public static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN = "//button[@data-click-action='resource.download' and contains(@data-params, '%s')]";

    public FilesPage(WebDriver driver) {
        super(driver);
    }

    public FilesPage open() {
        waitForElementIsClickable(FILES_LINK_LOCATOR).click();
        return this;
    }
}
