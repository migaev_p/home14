package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.common.bo.Account;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.InboxPage;
import com.gomel.tat.home14.lib.ui.screen.yandexmail.MailLoginPage;
import com.gomel.tat.home14.lib.report.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;

public class LoginMailService {

    public static InboxPage loginToMailAccount(WebDriver driver, Account account) {
        Logger.info("Login to mail.");
        return fillInputFields(driver, account).submitLogin();
    }

    public static MailLoginPage failureLoginToMailAccount(WebDriver driver, Account account) {
        Logger.info(" login to mail.");
        fillInputFields(driver, account);
        return submitLoginExpectingFailure(driver);
    }

    private static MailLoginPage submitLoginExpectingFailure(WebDriver driver) {
        MailLoginPage mailLoginPage = new MailLoginPage(driver);
        mailLoginPage.submitLogin();
        waitForElementIsVisible(MailLoginPage.WRONG_LOGIN_ERROR_LOCATOR);
        return mailLoginPage;
    }

    private static MailLoginPage fillInputFields(WebDriver driver, Account account) {
        String login = account.getLogin();
        String password = account.getPassword();
        Logger.info("Fill login input field.");
        Logger.debug("Login:" + login + " Password: " + password);
        MailLoginPage mailLoginPage = PageFactory.initElements(driver, MailLoginPage.class);
        mailLoginPage.typeUsername(login);
        mailLoginPage.typePassword(password);
        screenshot();
        return mailLoginPage;
    }
}
