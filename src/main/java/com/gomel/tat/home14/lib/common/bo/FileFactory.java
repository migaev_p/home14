package com.gomel.tat.home14.lib.common.bo;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.home14.lib.utils.Utils.getRandomContent;

public class FileFactory {
    private static final int CONTENT_SIZE = 200;
    private static final String TEMPORARY_FILE_PREFIX = "test";
    private static final String TEMPORARY_FILE_POSTFIX = ".txt";

    private static File getTestFile() {
        File testFile = null;
        try {
            testFile = File.createTempFile(TEMPORARY_FILE_PREFIX, TEMPORARY_FILE_POSTFIX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        putRandomContentToFile(testFile);
        return testFile;
    }

    public static List<File> getTestFiles(int numberOfTestFiles) {
        List<File> testFiles = new ArrayList<File>();
        for (int i = 0; i < numberOfTestFiles; i++) {
            testFiles.add(getTestFile());
        }
        return testFiles;
    }

    public static File putRandomContentToFile(File file) {
        try {
            PrintWriter printWriter = new PrintWriter(file.getAbsoluteFile());
            try {
                printWriter.print(getRandomContent(CONTENT_SIZE));
            } finally {
                printWriter.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }
}
