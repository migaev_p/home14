package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.FilesPage;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.TrashPage;
import com.gomel.tat.home14.lib.report.Logger;
import com.gomel.tat.home14.lib.utils.Utils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home14.lib.utils.Utils.*;

public class DiskService {
    public static final String CHROME_DOWNLOAD_EXTENSION = ".crdownload";
    public static final String FIREFOX_DOWNLOAD_EXTENSION = ".PART";

    public static FilesPage downloadFiles(WebDriver driver, List<File> testFiles) {
        for (File file : testFiles) {
            downloadFile(driver, file);
        }
        return new FilesPage(driver);
    }

    public static FilesPage downloadFile(WebDriver driver, File testFile) {
        String fileName = testFile.getName();
        Logger.info("Download file: " + fileName);
        By uploadedFileLocator = getLocatorByPattern(FilesPage.UPLOADED_FILE_LOCATOR_PATTERN, fileName);
        By downloadButtonLocator = getLocatorByPattern(FilesPage.DOWNLOAD_BUTTON_LOCATOR_PATTERN, fileName);
        waitForElementIsVisible(uploadedFileLocator).click();
        waitForElementIsVisible(downloadButtonLocator).click();
        Logger.debug("Click to download button. Locator: " + downloadButtonLocator);
        waitForDownloadFile(getFilePath(WebDriverHelper.DOWNLOAD_PATH, fileName), FilenameUtils.getExtension(fileName));
        Logger.debug("File downloaded: " + fileName);
        screenshot();
        return new FilesPage(driver);
    }

    private static String getDownloadExtension() {
        switch (GlobalConfig.getInstance().getBrowserType()) {
            case CHROME:
                Logger.debug("Chrome download extension");
                return CHROME_DOWNLOAD_EXTENSION;
            case FIREFOX:
                Logger.debug("Firefox download extension");
                return FIREFOX_DOWNLOAD_EXTENSION;
        }
        throw new InvalidParameterException("Download extension is not valid");
    }

    public static TrashPage restoreTestFilesFromTrash(WebDriver driver, List<File> testFiles) {
        for (File file : testFiles) {
            By restoreFileButtonLocator = getLocatorByPattern(TrashPage.RESTORE_FILE_BUTTON_LOCATOR_PATTERN, file.getName());
            Logger.info("Restore file: " + file.getName() + "Locator: " + restoreFileButtonLocator);
            DiskDeleteService.selectDeletedFile(driver, file);
            screenshot();
            waitForElementIsVisible(restoreFileButtonLocator).click();
            waitForElementIsVisible(TrashPage.RESTORE_NOTIFICATION_LOCATOR);
        }
        return new TrashPage(driver);
    }

    public static List<String> getCheckSumOfFile(List<File> files) {
        List<String> checkSums = new ArrayList<String>();
        for (File file : files) {
            try {
                String checkSum = Utils.getCheckSumOfFile(file);
                checkSums.add(checkSum);
                Logger.debug("Check sum: " + checkSum);
            } catch (Exception e) {
                Logger.error("Failed to get check sum: " + e.getMessage(), e);
                screenshot();
            }
        }
        return checkSums;
    }

    public static List<File> getDownloadedFiles(List<File> testFiles) {
        Logger.info("Get dowloaded files list.");
        List<File> expectedFiles = new ArrayList<File>();
        for (File file : testFiles) {
            Path resultFilePath = Paths.get(getFilePath(WebDriverHelper.DOWNLOAD_PATH, file.getName()));
            File expectedFile = resultFilePath.toFile();
            expectedFiles.add(expectedFile);
            Logger.debug("Add file to list: " + expectedFile.getName());
        }
        return expectedFiles;
    }

    public static FilesPage uploadTestFiles(WebDriver driver, List<File> testFiles) {
        for (File file : testFiles) {
            Logger.debug("Upload file: " + file.getName());
            uploadFile(driver, file);
        }
        return new FilesPage(driver);
    }

    public static FilesPage uploadFile(WebDriver driver, File testFile) {
        Logger.info("Upload file: " + testFile.getName());
        driver.findElement(FilesPage.INPUT_BUTTON_LOCATOR).sendKeys(getTestFilePath(testFile));
        waitForElementIsVisible(FilesPage.UPLOAD_DONE_LOCATOR);
        screenshot();
        waitForElementIsVisible(FilesPage.DIALOG_CLOSE_BUTTON_LOCATOR).click();
        return new FilesPage(driver);
    }
}
