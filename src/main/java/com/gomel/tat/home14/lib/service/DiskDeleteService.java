package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.FilesPage;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.TrashPage;
import com.gomel.tat.home14.lib.report.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.util.List;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home14.lib.utils.Utils.getLocatorByPattern;

public class DiskDeleteService {
    public static final int FIRST_LIST_INDEX = 0;

    public static TrashPage finalDeleteFiles(WebDriver driver, List<File> testFiles) {
        Logger.info("Delete file from trash box and wait while delete notification become visible.");
        for (File file : testFiles) {
            String fileName = file.getName();
            selectDeletedFile(driver, file);
            Logger.debug("Try to delete file: " + fileName);
            waitForElementIsVisible(getLocatorByPattern(TrashPage.DELETED_FILE_BUTTON_LOCATOR_PATTERN, fileName)).click();
            screenshot();
            Logger.debug("Wait while delete notification become visible. Locator: " + TrashPage.FINAL_DELETE_NOTIFICATION_LOCATOR);
            waitForElementIsVisible(TrashPage.FINAL_DELETE_NOTIFICATION_LOCATOR);
        }
        return new TrashPage(driver);
    }

    public static TrashPage selectDeletedFile(WebDriver driver, File file) {
        String fileName = file.getName();
        Logger.debug("Select file: " + fileName);
        waitForElementIsVisible(getLocatorByPattern(TrashPage.DELETED_FILE_LOCATOR_PATTERN, fileName)).click();
        return new TrashPage(driver);
    }

    public static TrashPage deleteFilesToTrash(WebDriver driver, List<File> testFiles) {
        Logger.info("Delete files to trash.");
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL);
        for (File file : testFiles) {
            String fileName = file.getName();
            By uploadedFileLocator = getLocatorByPattern(FilesPage.UPLOADED_FILE_LOCATOR_PATTERN, fileName);
            Logger.debug("Select file for delete: " + fileName + "File locator: " + uploadedFileLocator);
            screenshot();
            actions.click(driver.findElement(uploadedFileLocator));
        }
        Logger.info("Dropdown switched files to trash box...");
        actions.keyUp(Keys.CONTROL)
                .dragAndDrop(getDraggableFile(driver, testFiles), getTrash(driver))
                .build()
                .perform();
        screenshot();
        waitForElementIsVisible(FilesPage.DELETE_NOTIFICATION_LOCATOR);
        return new TrashPage(driver);
    }

    private static WebElement getDraggableFile(WebDriver driver, List<File> testFiles) {
        By uploadedFileLocator = getLocatorByPattern(FilesPage.UPLOADED_FILE_LOCATOR_PATTERN, getFirstTestFile(testFiles).getName());
        Logger.debug("Draggable file locator: " + uploadedFileLocator);
        return driver.findElement(uploadedFileLocator);
    }

    private static WebElement getTrash(WebDriver driver) {
        Logger.debug("Trashbox locator: " + FilesPage.TRASH_BOX_LOCATOR);
        WebDriverHelper.scrollDownPage();
        screenshot();
        return driver.findElement(FilesPage.TRASH_BOX_LOCATOR);
    }

    public static File getFirstTestFile(List<File> testFiles) {
        File firstTestFile = testFiles.get(FIRST_LIST_INDEX);
        Logger.debug("First test file: " + firstTestFile.getName());
        return firstTestFile;
    }
}
