package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.WebDriver;

public class TrashPage extends Page {
    public static final String partOfTrashUrl = "#trash";

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public TrashPage open() {
        driver.navigate().to(WebDriverHelper.getUrlBeforePageName(partOfTrashUrl));
        return this;
    }
}
