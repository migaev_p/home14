package com.gomel.tat.home14.lib.ui;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.report.Logger;
import com.gomel.tat.home14.lib.utils.Utils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.HashMap;

public class WebDriverHelper {
    public static final String DOWNLOAD_PATH = Utils.getTempDirectory();
    private static final String CHAR_BEFORE_NEEDED_PART_OF_URL = "#";
    private static final String SCREENSHOT_DESTINATION_DIRECTORY = "screenshot";
    private static final String SCREENSHOT_EXTENSION = ".png";
    private static final String NEVER_ASK_FILE_EXTENSION = "txt";
    private static final String FIREFOX_NEVER_ASK_SAVE_TO_DISK = "browser.helperapps.neverAsk.saveToDisk";
    private static final String FIREFOX_DOWNLOAD_DIRECTORY = "browser.download.dir";
    private static final String CHROME_NEVER_POPUP = "profile.default_content_settings.popups";
    private static final String CHROME_DOWNLOAD_DIRECTORY = "download.default_directory";
    private static final String CHROME_PREFERENCE = "prefs";
    private static final String CHROME_DRIVER = "webdriver.chrome.driver";
    private static final String CHROME_DRIVER_PATH = "./src/main/resources/chromedriver";
    private static final String URL_PREFIX = "http://";
    private static final String URL_POSTFIX = "/wd/hub";
    private static final String PORT_SEPARATOR = ":";
    private static final String REMOTE_RUN = "remote";
    private static final String LOCAL_RUN = "local";

    protected static WebDriver driver;

    public static WebDriver getWebDriver(BrowserType browser) {
        if (GlobalConfig.getInstance().getRunMethod().equals(REMOTE_RUN)) {
            switch (browser) {
                default:
                case CHROME:
                    Logger.debug("Start remote chrome browser");
                    driver = prepareRemoteDriver(setChromeCapabilities());
                    break;
                case FIREFOX:
                    Logger.debug("Start remote firefox browser");
                    driver = prepareRemoteDriver(setFirefoxCapabilities());
                    break;
            }
        } else if (GlobalConfig.getInstance().getRunMethod().equals(LOCAL_RUN)) {
            switch (browser) {
                default:
                case CHROME:
                    Logger.debug("Start local chrome browser");
                    System.setProperty(CHROME_DRIVER, CHROME_DRIVER_PATH);
                    driver = prepareLocalDriver(setChromeCapabilities());
                    break;
                case FIREFOX:
                    Logger.debug("Start local firefox browser");
                    driver = prepareLocalDriver(setFirefoxCapabilities());
                    break;
            }
        } else {
            throw new InvalidParameterException("Invalid run method");
        }
        manageWebDriver(driver);
        return driver;
    }

    public static WebDriver prepareRemoteDriver(DesiredCapabilities capabilities) {
        RemoteWebDriver remoteDriver = null;
        try {
            URL webDriverUrl = new URL(getRemoteWebDriverUrl());
            Logger.debug("Web driver URL: " + webDriverUrl);
            remoteDriver = new RemoteWebDriver(webDriverUrl, capabilities);
            Logger.debug("Remote web driver is created");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Logger.error("Web driver URL is malformed", e);
        }
        return remoteDriver;
    }

    private static WebDriver prepareLocalDriver(DesiredCapabilities capabilities) {
        return new ChromeDriver(capabilities);
    }

    private static DesiredCapabilities setChromeCapabilities() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put(CHROME_NEVER_POPUP, 0);
        chromePrefs.put(CHROME_DOWNLOAD_DIRECTORY, DOWNLOAD_PATH);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption(CHROME_PREFERENCE, chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
    }

    private static DesiredCapabilities setFirefoxCapabilities() {
        Logger.debug("Set firefox capabilities");
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.dir", "tmp/download");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.startup.homepage_override.mstone", "ignore");
        profile.setPreference("browser.startup.page", "0");
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        return capabilities;
    }

    public static void manageWebDriver(WebDriver driver) {
        Logger.debug("Manage web driver");
        driver.manage().window().maximize();
        WebDriverWaits.setPageLoadWait();
        WebDriverWaits.setImplicitlyWait();
    }

    public static String getRemoteWebDriverUrl() {
        return URL_PREFIX + GlobalConfig.getInstance().getSeleniumHub() +
                PORT_SEPARATOR + GlobalConfig.getInstance().getSeleniumPort() + URL_POSTFIX;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            Logger.error("Problem with shutting down driver: " + e.getMessage(), e);
        }
    }

    public static String getUrlBeforePageName(String finalPartOfUrl) {
        String currentUrl = driver.getCurrentUrl();
        return currentUrl.substring(0, currentUrl.indexOf(CHAR_BEFORE_NEEDED_PART_OF_URL)).concat(finalPartOfUrl);
    }

    public static void screenshot() {
        byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        File screenshotFile = new File(GlobalConfig.getInstance().getResultDir()
                + File.separator + SCREENSHOT_DESTINATION_DIRECTORY
                + File.separator + System.nanoTime() + SCREENSHOT_EXTENSION);
        try {
            org.apache.commons.io.FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Logger.save(SCREENSHOT_DESTINATION_DIRECTORY + File.separator + screenshotFile.getName());
        } catch (IOException e) {
            Logger.error("Failed to write screenshot: " + e.getMessage(), e);
        }
    }

    public static void scrollDownPage() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }
}
