package com.gomel.tat.home14.lib.common.bo;

import static com.gomel.tat.home14.lib.common.CommonConstants.DEFAULT_MAIL_TO_SEND;
import static com.gomel.tat.home14.lib.utils.Utils.getRandomContent;

public class LetterFactory {
    public static final String emptyString = "";
    private static final int CONTENT_SIZE = 200;
    private static final int SIZE_OF_SUBJECT = 10;
    private static final int SIZE_OF_WRONG_ADDRESS = 10;

    public static Letter getRandomLetter() {
        return new Letter(DEFAULT_MAIL_TO_SEND, getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getLetterWithWrongAddress() {
        return new Letter(getRandomContent(SIZE_OF_WRONG_ADDRESS), getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getEmptyLetter() {
        return new Letter(DEFAULT_MAIL_TO_SEND, emptyString, emptyString);
    }

    public static String getRandomMailSubject() {
        return "test subject: " + getRandomContent(SIZE_OF_SUBJECT);
    }

    public static String getRandomMailContent() {
        return "test content: " + getRandomContent(CONTENT_SIZE);
    }
}
