package com.gomel.tat.home14.lib.service;

import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.ui.screen.yandexdisk.TrashPage;
import com.gomel.tat.home14.lib.report.Logger;

import java.io.File;
import java.util.List;

import static com.gomel.tat.home14.lib.ui.WebDriverHelper.screenshot;
import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;
import static com.gomel.tat.home14.lib.utils.Utils.getFilePath;
import static com.gomel.tat.home14.lib.utils.Utils.getLocatorByPattern;

public class DiskCheckService {

    public static Boolean isFilesExists(List<File> testFiles) {
        Logger.info("Check is files exists.");
        Boolean result = true;
        for (File file : testFiles) {
            String fileName = file.getName();
            Logger.debug("Check file: " + fileName);
            File downloadedFile = new File(getFilePath(WebDriverHelper.DOWNLOAD_PATH, fileName));
            if (!downloadedFile.exists() || downloadedFile.isDirectory()) {
                Logger.debug("Downloaded file does't exists: " + downloadedFile);
                screenshot();
                result = false;
            }
            Logger.debug("File " + downloadedFile.getName() + " exists");
        }
        Logger.info("File exists: " + result);
        return result;
    }

    public static Boolean isFilesDisplayed(List<File> testFiles, String pattern) {
        Logger.info("Check is files displayed.");
        Boolean result = true;
        for (File file : testFiles) {
            String fileName = file.getName();
            Logger.info("Check file: " + fileName);
            Logger.info("File locator: " + getLocatorByPattern(pattern, fileName));
            if (!waitForElementIsVisible(getLocatorByPattern(pattern, fileName)).isDisplayed()) {
                Logger.debug("File does't exists: " + fileName);
                screenshot();
                result = false;
            }
        }
        Logger.info("Files displayed: " + result);
        return result;
    }

    public static Boolean isCheckSumsMatch(List<String> actualCheckSums, List<String> resultCheckSums) {
        Boolean result = actualCheckSums.containsAll(resultCheckSums);
        Logger.debug("Result of comparison check sums : " + result);
        return result;
    }

    public static Boolean finalDeleteCheck() {
        Logger.info("Check is notification displayed. Locator: " + TrashPage.FINAL_DELETE_NOTIFICATION_LOCATOR);
        screenshot();
        return waitForElementIsVisible(TrashPage.FINAL_DELETE_NOTIFICATION_LOCATOR).isDisplayed();
    }
}
