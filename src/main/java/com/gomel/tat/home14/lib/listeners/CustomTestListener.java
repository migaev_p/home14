package com.gomel.tat.home14.lib.listeners;

import com.gomel.tat.home14.lib.ui.WebDriverHelper;
import com.gomel.tat.home14.lib.report.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class CustomTestListener extends TestListenerAdapter {

    @Override
    public void onStart(ITestContext testContext) {
        super.onStart(testContext);
        Logger.info("[STARTED TEST] " + testContext.getName());
    }

    @Override
    public void onTestSuccess(ITestResult testResult) {
        super.onTestSuccess(testResult);
        Logger.info("Test success: " + testResult.getMethod().getMethodName());
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        super.onTestFailure(testResult);
        WebDriverHelper.screenshot();
        Logger.error("Test failed: " + testResult.getMethod().getMethodName() + ". Description:" + testResult.getMethod().getDescription(), testResult.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        super.onTestSkipped(testResult);
        Logger.error("Test skipped: " + testResult.getMethod().getMethodName() + ". Description:" + testResult.getMethod().getDescription(), testResult.getThrowable());
    }

    @Override
    public void onConfigurationFailure(ITestResult testResult) {
        super.onConfigurationFailure(testResult);
        Logger.error("Configuration failed: " + testResult.getMethod().getMethodName() + ". Description:" + testResult.getMethod().getDescription(), testResult.getThrowable());
    }

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        Logger.info("[FINISHED TEST] " + testContext.getName());
        Logger.info("Total test run: " + testContext.getAllTestMethods().length);
        Logger.info("Test failed: " + testContext.getFailedTests().size());
        Logger.info("Test skipped: " + testContext.getFailedTests().size());
        Logger.info("Configuration failed: " + testContext.getFailedConfigurations().size());
        WebDriverHelper.shutdownWebDriver();
    }
}
