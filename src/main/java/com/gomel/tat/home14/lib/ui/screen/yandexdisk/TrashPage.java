package com.gomel.tat.home14.lib.ui.screen.yandexdisk;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.*;

public class TrashPage extends Page {
    public static final By TRASH_BOX_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    public static final By TRASH_BOX_BUTTON_LOCATOR = By.xpath("//button[@data-click-action and contains(@data-params, 'trash')]");
    public static final By RESTORE_NOTIFICATION_LOCATOR = By.xpath("//div[contains(@class, 'item_moved')]");
    public static final By FINAL_DELETE_NOTIFICATION_LOCATOR = By.xpath("//div[contains(@class, 'notifications__text')]");
    public static final String DELETED_FILE_BUTTON_LOCATOR_PATTERN = "//div[contains(@class, 'icon')]//button[@data-click-action='resource.delete' and contains(@data-params, '%s')]";
    public static final String RESTORE_FILE_BUTTON_LOCATOR_PATTERN = "//div[contains(@class, 'icon')]//button[@data-click-action='resource.restore' and contains(@data-params, '%s')]";
    public static final String DELETED_FILE_LOCATOR_PATTERN = "//div[@title='%s' and @data-nb='resource']";

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public TrashPage open() {
        waitForElementIsClickable(TRASH_BOX_LOCATOR).click();
        waitForElementIsClickable(TRASH_BOX_BUTTON_LOCATOR).click();
        return this;
    }
}
