package com.gomel.tat.home14.lib.ui.screen.yandexmail;

import com.gomel.tat.home14.lib.ui.screen.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home14.lib.ui.WebDriverWaits.waitForElementIsVisible;

public class SpecifiedLetterPage extends Page {
    public static final By FIRST_LETTER_LINK_LOCATOR = By.xpath("//div[@class='block-messages-wrap' and not(ancestor::div[contains(@style,'display: none')])]//div[@class='b-messages']/div[1]//a");
    public static final By MAIL_SUBJECT_LOCATOR = By.xpath("//div[@class='block-message b-message_loaded' and not(child::div[contains(@style,'display: none')])]//span[contains(@class, 'js-message-subject')]");
    //public static final By MAIL_RECIPIENT_LOCATOR = By.xpath("//div[@class='block-message-head' and not(ancestor::div[contains(@style,'display: none')])]//div[contains(@class, 'field_field')]//span[@class='b-message-head__name']");
    public static final By MAIL_BODY_LOCATOR = By.xpath("//div[@class='block-message-body' and not(ancestor::div[contains(@style,'display: none')])]//div[@class='b-message-body']/div[@class='b-message-body__content']/div");

    public SpecifiedLetterPage(WebDriver driver) {
        super(driver);
    }

    public SpecifiedLetterPage openFirstLetter() {
        waitForElementIsVisible(FIRST_LETTER_LINK_LOCATOR).click();
        return this;
    }
}
