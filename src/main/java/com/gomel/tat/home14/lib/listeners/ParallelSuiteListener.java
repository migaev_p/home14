package com.gomel.tat.home14.lib.listeners;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class ParallelSuiteListener implements ISuiteListener {

    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }

    public void onFinish(ISuite suite) {

    }
}
