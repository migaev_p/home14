package com.gomel.tat.home14.lib.common.bo;

public class Letter {
    private String recipient;
    private String subject;
    private String body;

    public Letter(String recipient, String subject, String body) {
        setRecipient(recipient);
        setSubject(subject);
        setBody(body);
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "recipient='" + recipient + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
