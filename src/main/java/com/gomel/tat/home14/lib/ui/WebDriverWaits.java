package com.gomel.tat.home14.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WebDriverWaits extends WebDriverHelper {
    public static final int PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 20;
    public static final int CLICKABLE_TIMEOUT_SECONDS = 10;
    public static final int VISIBLE_TIMEOUT_SECONDS = 15;

    public static void setPageLoadWait() {
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public static void setImplicitlyWait() {
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, CLICKABLE_TIMEOUT_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, VISIBLE_TIMEOUT_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
