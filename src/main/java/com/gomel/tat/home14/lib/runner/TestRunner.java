package com.gomel.tat.home14.lib.runner;

import com.gomel.tat.home14.lib.config.GlobalConfig;
import com.gomel.tat.home14.lib.listeners.ExtentReportListener;
import com.gomel.tat.home14.lib.listeners.ParallelSuiteListener;
import com.gomel.tat.home14.lib.report.Logger;
import com.gomel.tat.home14.lib.listeners.TestNgLoggerListener;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.IReporter;
import org.testng.ITestListener;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestRunner {

    public TestRunner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        new TestRunner(args).runTests();
    }

    private void parseCli(String[] args) {
        Logger.info("Parse cli params...");
        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
        try {
            parser.parseArgument(args);
            Logger.info(GlobalConfig.getInstance().toString());
        } catch (CmdLineException e) {
            Logger.error("Failed to parse cli params: " + e.getMessage(), e);
            parser.printUsage(System.out);
            System.exit(1);
        }
    }

    private void runTests() throws IOException, SAXException, ParserConfigurationException {
        TestNG testNG = new TestNG();
        addListeners(testNG);
        configureSuites(testNG);
        Logger.info("Tests will be started");
        testNG.run();
    }

    private void addListeners(TestNG testNG) {
        testNG.addListener(new ParallelSuiteListener());
        testNG.addListener((IReporter) new ExtentReportListener());
        testNG.addListener((ITestListener) new ExtentReportListener());
        testNG.addListener(new TestNgLoggerListener());
    }

    private void configureSuites(TestNG testNG) throws ParserConfigurationException, SAXException, IOException {
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        for (String suitePath : GlobalConfig.getInstance().getSuites()) {
            InputStream suiteInClassPath = getSuiteInputStream(suitePath);
            if (suiteInClassPath != null) {
                suites.addAll(new Parser(suiteInClassPath).parse());
            } else {
                suites.addAll(new Parser(suitePath).parse());
            }
        }
        for (XmlSuite xmlSuite : suites) {
            testNG.setCommandLineSuite(xmlSuite);
        }
    }

//    public void parseCli(String[] args) {
//        CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
//        try {
//            parser.parseArgument(args);
//        } catch (CmdLineException e) {
//            Logger.error("Parse argument failed " + e.getMessage(), e);
//            parser.printUsage(System.err);
//        }
//    }

    private InputStream getSuiteInputStream(String suite) {
        return this.getClass().getClassLoader().getResourceAsStream(suite);
    }
}
