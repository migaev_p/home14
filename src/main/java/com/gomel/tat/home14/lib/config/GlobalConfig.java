package com.gomel.tat.home14.lib.config;

import com.gomel.tat.home14.lib.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite;

import java.util.List;

public class GlobalConfig {

    @Option(name = "-bt", usage = "Sets a browser (chrome or firefox)", required = true)
    private BrowserType browserType = BrowserType.CHROME;

    @Option(name="-suites", usage = "list of paths to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;

    @Option(name = "-hub", usage = "selenium hub")
    private String seleniumHub = "localhost";

    @Option(name = "-port", usage = "selenium port")
    private int seleniumPort = 4444;

    @Option(name = "-pm", usage = "parallel mode: false or tests")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.FALSE;

    @Option(name="-tc", usage = "amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-run", usage = "run method: local or remote")
    private String runMethod = "remote";

    @Option(name = "-result_dir", usage = "Directory to put results")
    private String resultDir = "results";

    private static GlobalConfig instance;

    private GlobalConfig() {
    }

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public String getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(String seleniumHub) {
        this.seleniumHub = seleniumHub;
    }

    public int getSeleniumPort() {
        return seleniumPort;
    }

    public void setSeleniumPort(int seleniumPort) {
        this.seleniumPort = seleniumPort;
    }

    public XmlSuite.ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(XmlSuite.ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public String getRunMethod() {
        return runMethod;
    }

    public String setRunMethod() {
        return this.runMethod = runMethod;
    }

    public String getResultDir() {
        return resultDir;
    }

    public void setResultDir(String resultDir) {
        this.resultDir = resultDir;
    }

    @Override
    public String toString() {
        return "GlobalConfig{" +
                "browserType=" + browserType +
                ", suites=" + suites +
                ", seleniumHub='" + seleniumHub + '\'' +
                ", seleniumPort=" + seleniumPort +
                ", parallelMode=" + parallelMode +
                ", threadCount=" + threadCount +
                ", runMethod='" + runMethod + '\'' +
                ", resultDir='" + resultDir + '\'' +
                '}';
    }
}